---
layout: page
title: About
permalink: /about/
---
<div class="container">
<article>
	<img src="/img/jekylltux.png" style="float: left; width: 200px; transform: translate(-20px, -30px);">
	<p>Este meu site pessoal é um projeto de aprendizado web, nele utilizo 
	como ferramenta o 
  <a href="https://jekyllrb.com/">Jekyll</a>
   e <a href="https://shopify.github.io/liquid/">Liquid template</a>.</p>
  <p>E hospedagem utilizando o 
  <a href="https://about.gitlab.com/features/pages/">
  GitLab Pages
  <i class="fa fa-gitlab" aria-hidden="true" style="color: #FC6D26;"></i>
  </a>.
  </p> 
</article>
</div>