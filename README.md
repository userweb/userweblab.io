# [https://userweb.gitlab.io/](https://userweb.gitlab.io/)

This is the code repository for my personal website 

## `under construction`


```sh

```
Based on:
- [dylanaraps.com](https://github.com/dylanaraps/dylanaraps.github.io) theme

This use:
- [Jekyll](https://jekyllrb.com/) as static site generator
- [Hover.css](https://ianlunn.github.io/Hover/) for hover animations effects
- [Font Awesome](https://fontawesome.com/v4.7.0/) for icons
- [Pixabay](https://pixabay.com/) for some [CC0 Creative Common](https://creativecommons.org/publicdomain/zero/1.0/) images