---
layout: post
title: "Como bloquear dominios"
date: 2018-07-14 22:15:48
image: "/img/config.png"
categories: Dicas
tags: [redes, config]
---
Neste post mostrarei como usar o computador, através do arquivo **hosts** para bloquear algum dominio, site, impedindo que deste envie algum dado.

**Unix/Linux/BSD**

Localização do arquivo **hosts**:
 `/etc/hosts`

Abra o arquivo hosts com seu editor de texto, requer permissão o que pode ser feito com sudo:
```sh
$ sudo <editor> /etc/hosts
```
> Você não deve remover nada do arquivo, somente, se não souber o que está fazendo, não remova nem altere nenhuma das linhas já existentes. Basta  somente adicionar as novas linhas como exemplo abaixo com o dominio que deseja bloquear.

```sh
#dominios bloqueados                         

0.0.0.0	www.google-analytics.com
0.0.0.0	google-analytics.com
0.0.0.0	ssl.google-analytics.com
```

* O ip 0.0.0.0 é um enderaçamento não roteável a nenhum destino na sua máquina.
* Não há problema em ter linhas em branco ou linhas comentadas antes. Comentários são indicados por um '#'
* Pode ser feito para bloquear dominios de analytics, telemetria ou pornografia, etc...

O que o `/etc/hosts` faz é redirecionar o nome domínio para o endereçamento ip correspondente ou seja, acima esta dizendo para o computador, que o ip de tal sites é a sua própria maquina, como não vai encontrar nada, irá ignora-los bloqueando-os efetivamente.

----------
Abaixo é um exemplo como é o meu `/etc/hosts`:
```sh
$ cat /etc/hosts
127.0.0.1	localhost
127.0.1.1	Linux
::1	localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters

# block access to websites:

0.0.0.0   redshell.io api.redshell.io
0.0.0.0   treasuredata.com api.treasuredata.com
0.0.0.0   cdn.rdshll.com

0.0.0.0   config.uca.cloud.unity3d.com
0.0.0.0   cdp.cloud.unity3d.com

0.0.0.0   www.google-analytics.com
0.0.0.0   google-analytics.com
0.0.0.0   ssl.google-analytics.com

#100::/64   #IPv6 Discard prefix

```
